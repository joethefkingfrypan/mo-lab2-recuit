#pragma once

#include "Entete.h"

#include <sstream>
#include <string>

class Parameters
{
	public:
		Parameters(const int temperature, const double alpha, const int steps, const int numberOfEvaluations);
		~Parameters(void);
		void setupRecuit(TRecuit &recuit) const;
		string Parameters::toString() const;

	private:
		double temperature;
		double alpha;
		int steps;
		int numberOfEvaluations;
};

