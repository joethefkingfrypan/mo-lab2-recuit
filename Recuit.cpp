#include <windows.h>

#include "Entete.h"
#include "Parameters.h"

#include <stdlib.h>
#include <time.h>
#include <cmath>
#include <vector>

#pragma comment (lib,"RecuitDLL.lib")

//%%%%%%%%%%%%%%%%%%%%%%%%% IMPORTANT: %%%%%%%%%%%%%%%%%%%%%%%%% 
//Le fichier de probleme (.txt) doit se trouver dans le rÃ©pertoire courant du projet pour une exÃ©cution Ã  l'aide du compilateur.
//Les fichiers de la DLL (GeneticDLL.dll et GeneticDLL.lib) doivent se trouver dans le mÃªme rÃ©pertoire que l'exÃ©cutable (.exe-DEBUG) et 
//dans le rÃ©pertoire courant du projet pour une exÃ©cution Ã  l'aide du compilateur.
//Indiquer les arguments du programme dans les propriÃ©tÃ©s du projet - dÃ©bogage - arguements.
//Sinon, utiliser le rÃ©pertoire execution.

//*****************************************************************************************
// Prototype des fonctions se trouvant dans la DLL 
//*****************************************************************************************
//DESCRIPTION:	Lecture du Fichier probleme et initialiation de la structure Problem
extern "C" _declspec(dllimport) void LectureProbleme(std::string FileName, TProblem & unProb, TRecuit &unRecuit);

//DESCRIPTION:	Fonction d'affichage Ã  l'Ã©cran permettant de voir si les donnÃ©es du fichier problÃ¨me ont Ã©tÃ© lues correctement
extern "C" _declspec(dllimport) void AfficherProbleme (TProblem unProb);

//DESCRIPTION: Affichage d'une solution a l'Ã©cran pour validation
extern "C" _declspec(dllimport) void AfficherSolution(const TSolution uneSolution, int N, std::string Titre);

//DESCRIPTION: Affichage Ã  l'Ã©cran de la solution finale, du nombre d'Ã©valuations effectuÃ©es et de certains paramÃ¨tres
extern "C" _declspec(dllimport) void AfficherResultats (const TSolution uneSol, TProblem unProb, TRecuit unRecuit);

//DESCRIPTION: Affichage dans un fichier(en append) de la solution finale, du nombre d'Ã©valuations effectuÃ©es et de certains paramÃ¨tres
extern "C" _declspec(dllimport) void AfficherResultatsFichier (const TSolution uneSol, TProblem unProb, TRecuit unRecuit, std::string FileName);

//DESCRIPTION:	Ã‰valuation de la fonction objectif d'une solution et MAJ du compteur d'Ã©valuation. 
//				Retourne un long reprÃ©sentant la distance totale de la trounÃ©e incluant le retour un point initial
extern "C" _declspec(dllimport) long EvaluerSolution(const TSolution uneSolution, TProblem unProb, TRecuit &unRecuit);

//DESCRIPTION:	CrÃ©ation d'une sÃ©quence alÃ©atoire de parcours des villes et Ã©valuation de la fonction objectif. Allocation dynamique de mÃ©moire
// pour la sÃ©quence (.Seq)
extern "C" _declspec(dllimport) void CreerSolutionAleatoire(TSolution & uneSolution, TProblem unProb, TRecuit &unRecuit);

//DESCRIPTION: Copie de la sÃ©quence et de la fonction objectif dans une nouvelle TSolution. La nouvelle TSolution est retournÃ©e.
extern "C" _declspec(dllimport) void CopierSolution (const TSolution uneSol, TSolution &Copie, TProblem unProb);

//DESCRIPTION:	LibÃ©ration de la mÃ©moire allouÃ©e dynamiquement
extern "C" _declspec(dllimport) void LibererMemoireFinPgm (TSolution uneCourante, TSolution uneNext, TSolution uneBest, TProblem unProb);

//*****************************************************************************************
// Prototype des fonctions locales 
//*****************************************************************************************

//DESCRIPTION:	CrÃ©ation d'une solution voisine Ã  partir de la solution uneSol. NB:uneSol ne doit pas Ãªtre modifiÃ©e
TSolution GetSolutionVoisine (const TSolution uneSol, TProblem unProb, TRecuit &unRecuit);

//DESCRIPTION:	 Echange de deux villes sÃ©lectionnÃ©e alÃ©atoirement. NB:uneSol ne doit pas Ãªtre modifiÃ©e
TSolution	Echange			(const TSolution uneSol, TProblem unProb, TRecuit &unRecuit);

//DESCRIPTION: Ensemble des méthodes liées à la génération de cas de tests
vector<Parameters> generateSuiteForTemperature(const int numberOfEvaluations);
vector<Parameters> generateSuiteForAlpha(const int numberOfEvaluations);
vector<Parameters> generateSuiteForSteps(const int numberOfEvaluations);

// MODIFIED METHODS
void runTestsSuite(const Type type, TRecuit &recuit, TSolution &current, TSolution &next, TSolution &best, TProblem &instance, const int numberOfEvaluations);
boolean runAlgorithmWithParameters(TRecuit &recuit, TSolution &current, TSolution &next, TSolution &best, TSolution &overallBest, TProblem &instance, const Parameters &params);
void invokeRS(TRecuit &recuit, TSolution &current, TSolution &next, TSolution &best, const TProblem &instance);
void decreaseTemperature (TRecuit &recuit);
double generateRandomValue();

//******************************************************************************************
// Fonction main
//*****************************************************************************************

vector<Parameters> generateSuiteForTemperature(const int numberOfEvaluations)
{
	vector<Parameters> runCases;
	runCases.push_back(Parameters(5, 0.99, 10, numberOfEvaluations));
	runCases.push_back(Parameters(8, 0.99, 10, numberOfEvaluations));
	runCases.push_back(Parameters(10, 0.99, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.99, 10, numberOfEvaluations));
	runCases.push_back(Parameters(12, 0.99, 10, numberOfEvaluations));
	runCases.push_back(Parameters(13, 0.99, 10, numberOfEvaluations));
	runCases.push_back(Parameters(14, 0.99, 10, numberOfEvaluations));
	return runCases;
}

vector<Parameters> generateSuiteForAlpha(const int numberOfEvaluations)
{
	vector<Parameters> runCases;
	runCases.push_back(Parameters(11, 0.99, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.90, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.80, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.70, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.60, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.50, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.40, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.30, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.20, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.10, 10, numberOfEvaluations));
	return runCases;
}

vector<Parameters> generateSuiteForSteps(const int numberOfEvaluations)
{
	vector<Parameters> runCases;
	runCases.push_back(Parameters(11, 0.99, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.90, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.80, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.70, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.60, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.50, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.40, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.30, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.20, 10, numberOfEvaluations));
	runCases.push_back(Parameters(11, 0.10, 10, numberOfEvaluations));
	return runCases;
}

int main(int NbParam, char *Param[])
{
	TSolution Courante;		//Solution active au cours des itÃ©rations
	TSolution Next;			//Solution voisine retenue Ã  une itÃ©ration
	TSolution Best;			//Meilleure solution depuis le dÃ©but de l'algorithme
	TProblem LeProb;		//DÃ©finition de l'instance de problÃ¨me
	TRecuit LeRecuit;		//DÃ©finition des paramÃ¨tres du recuit simulÃ©
	string NomFichier;	

	// Lecture des paramètres & du fichier de donnees
	NomFichier.assign(Param[1]);
	LectureProbleme(NomFichier, LeProb, LeRecuit);

	double numberOfEvaluations = atoi(Param[5]);
	
	runTestsSuite(TEMPERATURE, LeRecuit, Courante, Next, Best, LeProb, numberOfEvaluations);
	runTestsSuite(ALPHA, LeRecuit, Courante, Next, Best, LeProb, numberOfEvaluations);
	runTestsSuite(STEPS, LeRecuit, Courante, Next, Best, LeProb, numberOfEvaluations);

	return 0;
}

void runTestsSuite(const Type type, TRecuit &recuit, TSolution &current, TSolution &next, TSolution &best, TProblem &instance, const int numberOfEvaluations)
{
	unsigned int bestConfiguration = 0;
	vector<Parameters> runCases;
	string label;

	TSolution overallBest;
	overallBest.FctObj = -1;

	switch(type) {
		case TEMPERATURE:
			label = "TEMPERATURE";
			runCases = generateSuiteForTemperature(numberOfEvaluations);
			break;

		case ALPHA:
			label = "ALPHA";
			runCases = generateSuiteForAlpha(numberOfEvaluations);
			break;

		case STEPS:
			label = "STEPS";
			runCases = generateSuiteForSteps(numberOfEvaluations);
			break;
	}

	for(unsigned int i=0; i<runCases.size(); i++) {
		cout << endl << "	=============== NEW TEST FOR " << label << " ===============" << endl;
		if(runAlgorithmWithParameters(recuit, current, next, best, overallBest, instance, runCases[i])) {
			bestConfiguration = i;
		}
	}

	cout << endl << "	=============== OVERALL BEST CONFIGURATION FOR " << label << " ===============" << endl;
	cout << runCases[bestConfiguration].toString();
	cout << endl << "	==========================================================================" << endl << endl;
	
	system("PAUSE");
}

boolean runAlgorithmWithParameters(TRecuit &recuit, TSolution &current, TSolution &next, TSolution &best, TSolution &overallBest, TProblem &instance, const Parameters &params)
{
	boolean achievedBestOverallScore = false;
	params.setupRecuit(recuit);
	
	// Création de la solution initiale et initialisation de la "meilleure" solution
	CreerSolutionAleatoire(current, instance, recuit);
	best = current;
	if(overallBest.FctObj == -1) {
		overallBest = current;
	}

	// Invoke RS algorithm
	invokeRS(recuit, current, next, best, instance);

	AfficherResultats(best, instance, recuit);
	AfficherResultatsFichier(current, instance, recuit,"Resultats.txt");

	if(best.FctObj < overallBest.FctObj) {
		achievedBestOverallScore = true;
		overallBest = best;
	}
	
	LibererMemoireFinPgm(current, next, best, instance);

	return achievedBestOverallScore;
}

//DESCRIPTION: CrÃ©ation d'une solution voisine Ã  partir de la solution uneSol qui ne doit pas Ãªtre modifiÃ©e.
//Dans cette fonction, on appel le TYPE DE VOISINAGE sÃ©lectionnÃ© + on dÃ©termine la STRATÃ‰GIE D'ORIENTATION. 
//Ainsi, si la RÃˆGLE DE PIVOT nÃ©cessite l'Ã©tude de plusieurs voisins, la fonction TYPE DE VOISINAGE sera appelÃ©e plusieurs fois.
//Le TYPE DE PARCOURS dans le voisinage interviendra normalement dans la fonction TYPE DE VOISINAGE.
TSolution GetSolutionVoisine (const TSolution uneSol, TProblem unProb, TRecuit &unRecuit)
{
	//Type de voisinage : Ã  indiquer (Echange 2 villes alÃ©atoires)
	//Parcours dans le voisinage : Ã  indiquer	(Aleatoire)
	//RÃ¨gle de pivot : Ã  indiquer	(First-Impove)

	TSolution unVoisin;
	
	unVoisin = Echange(uneSol, unProb, unRecuit);
	return (unVoisin);	
}

//DESCRIPTION: Echange de deux villes sÃ©lectionnÃ©es alÃ©atoirement
TSolution Echange (const TSolution uneSol, TProblem unProb, TRecuit &unRecuit)
{
	int PosA, PosB, Tmp;
	TSolution Copie;
	
	//Utilisation d'une nouvelle TSolution pour ne pas modifier La solution courante (uneSol)
	CopierSolution(uneSol, Copie, unProb);
	
	//Tirage alÃ©atoire des 2 positions
	PosA = rand() % unProb.Nb;
	do
	{
		PosB = rand() % unProb.Nb;
	}while (PosA == PosB);
	//Verification pour ne pas perdre une Ã©valuation
	
	//Echange des 2 positions
	Tmp = Copie.Seq[PosA];
	Copie.Seq[PosA] = Copie.Seq[PosB];
	Copie.Seq[PosB] = Tmp;

	//Le nouveau voisin doit Ãªtre Ã©valuÃ© 
	Copie.FctObj = EvaluerSolution(Copie, unProb, unRecuit);
	return(Copie);
}

// ========================================================================================== //
//                                      MODIFIED METHODS                                      //
// ========================================================================================== //

void invokeRS(TRecuit &recuit, TSolution &current, TSolution &next, TSolution &best, const TProblem &instance)
{
	int stepNumber = 0;
	int iterationNumber = 0;
	int iterationPerStep = recuit.NB_EVAL_MAX / recuit.NbPalier;
	
	do // For each temperature step, do the following
	{
		do // For each iteration at a specific temperature
		{
			stepNumber++;
			// Get neighbor solution and compute result difference
			next = GetSolutionVoisine(current, instance, recuit);
			recuit.Delta = next.FctObj - current.FctObj;

			// Check if there is any improvement (delta < 0)
			//	 - [Improvement]: override current result in any case and override best result only if current result is better than all-time best
			//   - [No improvement]: Generate a random number, and if it is smaller than computed acceptance probability, override current result despite result degradation
			if (recuit.Delta <= 0) {
				current = next;
				if(next.FctObj < best.FctObj) {
					best = next;
					cout << "		[ITERATION #" << stepNumber << "] [TEMP = " << recuit.Temperature << "] New best is now " << best.FctObj << " (evals: " << recuit.CptEval << " / " << recuit.NB_EVAL_MAX << ")" << endl;
				}
			} else {
				double acceptanceProbability = exp(-recuit.Delta/recuit.Temperature);
				if(generateRandomValue() < acceptanceProbability) {
					current = next;
				}
			}
		} while(iterationNumber++ < iterationPerStep && recuit.CptEval < recuit.NB_EVAL_MAX);
		
		// At the end of each cycle, reset iterationNumber and decrease temperature
		iterationNumber = 0;		
		decreaseTemperature(recuit);

	} while (recuit.CptEval < recuit.NB_EVAL_MAX);
}

// Method used to decrease current temperature using alpha factor
void decreaseTemperature (TRecuit &recuit)
{
	recuit.Temperature = recuit.Temperature * recuit.Alpha;
}

// Method used to generate a floating point value in range [0-1] (both included)
double generateRandomValue() 
{
	return (rand()%101)/100.0f;
}